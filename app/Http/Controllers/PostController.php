<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
class PostController extends Controller
{
    public function index()
    {
       $posts = Post::all();
       return new PostResource(Post::all());
    }

    public function show($id)
    {
        $post = Post::find($id);
        return new PostResource(Post::findOrFail($id));
    }
}
