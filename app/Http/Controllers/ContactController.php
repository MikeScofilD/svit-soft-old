<?php

namespace App\Http\Controllers;

use App\Models\FormContact;
use Illuminate\Http\Request;
use App\Http\Resources\ContactResource;

class ContactController extends Controller
{

    public function store(Request $request)
    {
        return new ContactResource(FormContact::insert($request));
    }
    //request validate ||  php artisan make:request FormRequest
}
