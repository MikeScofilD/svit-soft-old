<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use App\Http\Resources\AuthorResource;

class AuthorController extends Controller
{
    public function index()
    {
        $authors = Author::all();
        dd($authors);
        return new AuthorResource($authors);
    }

    public function show($id)
    {
        $author = Author::find($id);
        dd($author);
        return new AuthorResource(Author::findOrFail($id));
    }
}
